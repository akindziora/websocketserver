/**
 * nd server 2014 by alexander Kindziora
 * 
 */
module.exports = function(self) {

    self.admin_rooms = [
        'userliste',
        'jobstatus'
    ];

    self.name = 'my_example_mysql_redis';

    self.initAuth = function(config) {
        var redis = require("redis");
        self.redisclient = redis.createClient();
        self.authmodel = self.getModel('redis/Session', self.redisclient);

        self.myauth = new config.auth(self, self.io);
        //console.log('redis/Session');
        return true;
    };

    /**
     * constructor to initialize used models and local classes
     */
    this.constructor = function() {

        self.constructor({
            user: require('./controller/user') // REGISTER USER CONTROLLER 
        }); //call parent constructor

        self.MODEL.job = self.getModel('Job');
        self.MODEL.user = self.getModel('User');
        self.MODEL.user_roles = self.getModel('UserRoles');

        return self;
    };

    /**
     * 
     */
    self.pre = {
        /*  'join': function(room) {
         
         return room;
         }*/

    };


    /**
     * magic function that executes every time before a __bind method was called
     * it returns the data that will be passed to __bind[METHODNAME](data)
     */
    self.__before = function(data, type) {
        /**
         * i can manipulate data if i want to
         */

        if (typeof self.pre[type] !== 'undefined') {
            data = self.pre[type](data);
        }

        return {
            'data': data,
            'success': true
        };

    };
    /**
     * magic __bind methods that get executed if triggered on client side
     */
    self.__execute = {
        ping: function(data, socket) {
            var allClientsCount = self.io.sockets.clients().length, clientCount = 0;

            Object.keys(self.client).forEach(function(SID) {

                Object.keys(self.client[SID].socket).forEach(function(sD) {

                    clientCount++;
                    self.client[SID].socket[sD].active = false;

                    if (clientCount >= allClientsCount) {
                        
                    }

                });

            });
            socket.get('phpsession', function(ull, sessionID) {
                self.client[sessionID].socket[socket.id].active = data;

                //console.log('ping erhalten von ' + self.client[sessionID].guruId, self.io.sockets.clients('userliste'));

                self.io.sockets.in('userliste').emit('userliste', self.client);
            });


        },
        join: function(room, socket) {



            if (self.admin_rooms.indexOf(room) !== -1) {
                socket.get('phpsession', function(ull, sessionID) {
                    self.MODEL.user_roles.findByUserId(self.client[sessionID].guruId, function(data) {

                        //console.log(data);

                        if (typeof data !== 'undefined') {
                            //console.log('join auth channel', room);
                            socket.join(room);
                            //console.log('users in channel', self.io.sockets.clients(room));
                        }
                    });
                });

            } else {
                //console.log('join open channel', room);
                socket.join(room);
            }

        },
        leave: function(room, socket) {
            //console.log('leaving channel', room);
            socket.leave(room);
        },
        msg: function(data, socket) {
            //console.log('sending message');

            if ('userliste' === data.room) {
                self.io.sockets.in(data.room).emit('userliste', self.client);
                //console.log('sending updated clientlist');
            } else {
                self.io.sockets.in(data.room).emit('msg', data);
            }

        },
        /**
         * find job by id and return job to client
         */
        getJob: function(data, socket) {
            self.MODEL.job.getById(data.job.meinjob.id, function(rows, fields) {
                socket.broadcast.emit('getJob', rows);
            });
        },
        /**
         * find user by name and return user to client
         * with own mysql class
         */
        getUser: function(data, socket) {

            //console.log(data);
            this.result = function(entry) {
                socket.emit('usernotice', {
                    'result': entry,
                    'keyword': data.name
                });
            };
            /* execute search on user table*/
            self.MODEL.user.findByName(data.name, this.result);
        },
        getClientList: function(data, socket) {
            //console.log('clientlist', self.client);
            socket.emit('userliste', self.client);
        },
        privateMessage: function(data, socket) {
            //console.log(self.io.sockets.sockets, data.user);
            if (g.isset(self.io.sockets.sockets[data.user])) {
                self.io.sockets.sockets[data.user].emit('result', data.msg);
            } else {
                //console.log('sending message failed, client not found', data.user);
            }
        }
    };
    return this.constructor();
};