/**
 * g server 2012 by alexander Kindziora
 * config file
 */
Sequelize = require("sequelize");
module.exports = {
    'path': 'my_example_mysql_app',
    'db': {
        type: 'mysql',
        connection: new Sequelize(
                'crowdmodul', //db
                'root', //user
                'root', //pass
                {
                    'host': 'localhost'
                })
    },
    'pathtokey': '/etc/apache2/ssl/crowdguru.de/20130916/crowdguru.key',
    'pathtocert': '/etc/apache2/ssl/crowdguru.de/20130916/crowdguru.crt',
    'pathtoca': '/etc/apache2/ssl/crowdguru.de/20130916/rapid.crt',
    'httplayer': 'https',
    'port': 18111,
    'auth': require('../core/auth')
};