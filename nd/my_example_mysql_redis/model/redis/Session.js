module.exports = function(self) {

    //console.log('redis', self.db.get);

    var db = self.db;
    self.name = 'session';

    self.mydb = {
        find: function(q) {
            return {
                'success': function(cb) {
                    db.get("PHPREDIS_SESSION:" + q.where.session_id, function(err, reply) {
                        //console.log('error', err, reply);
                        cb(reply);
                    });
                },
                'error': function(cb) {
                    //  parent.db.on("error", cb);
                }
            };
        }
    };

    return self.constructor();
};