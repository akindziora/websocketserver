/**
 * g server 2012 by alexander Kindziora
 * 
 */

module.exports = function(config) {
    var self = this, // public context
            _ = this; // private context

    g = new require('./g')(); // global functions singleton
    self.MODEL = {};
    self.session = {};
    self.client = {};
    var path = require('path');
    /**
     * init database
     */
    self.initDb = function(cb) {

        self.db = config.db.connection;
        if (g.isset(self.db.open)) {
            self.db.open(function(err, p_client) {
                self.client = p_client;
                //console.log('db connected');
                cb(err, p_client);
            });
        } else {
            cb();
        }

        return true;
    };
    /**
     * init authentication
     */
    self.initAuth = function(config) {
        //console.log('core/auth');
        self.myauth = new config.auth(self);
        return true;
    };
    /*
     * init sockserver
     */
    self.initServer = function() {

        var express = require('express');
        self.server = express();

        var fs = require('fs');

        for (var name in self.controller) {
            self.controller[name] = new self.controller[name](new require('./controller')(self.server));
        }

        self.server.set("jsonp callback", true);

        fs.readFile('../client/widgets/userliste/html/userliste.html', {
            'encoding': 'utf8'
        }, function(err, html) {

            if (err) {
                throw err;
            }
            self.server.get("/widget/:name/:template", function(req, res) {

                // var jsonpCallback = req.query.jsonp; //Assuming you are using express  

                res.jsonp({"html": html});
            });

        });

        self.server.configure(function() {

            // self.server.use(express.logger('dev'));
            self.server.use(express.static(__dirname + '/../client'));
            self.server.use(express.cookieParser());

            /*
             var RedisStore = require('connect-redis')(express);
             var sessionStore = new RedisStore;
             
             app.use(express.session({secret: "keyboard cat", store: sessionStore}));
             */

            self.server.use(function(req, res, next) {
                var sess = req.session;
                //console.log(sess);
            });

            self.server.use(express.errorHandler({
                showStack: true,
                dumpExceptions: true
            }));

            self.server.set('view options', {pretty: true});


        });
        var options = {};
        var http = require(config.httplayer);

        if (config.httplayer === 'https') {
            options = {
                key: fs.readFileSync(config.pathtokey),
                cert: fs.readFileSync(config.pathtocert),
                requestCert: true
            };
            self.serverHttp = http.createServer(options, self.server);
        } else {
            self.serverHttp = http.createServer(self.server);
        }



        self.serverHttp.listen(config.port);

        self.io = require('socket.io').listen(self.serverHttp);
        //console.log('self.io created');
        self.io.set("transports", ["websocket"]);

        return true;
    };
    self.__execute = { /*'testevent' : function (socket) {}*/};
    /*
     * init sockserver
     */
    self.bindMethods = function() {

        /**
         * execute pre and call binding
         */
        self.io.sockets.on('connection', function(socket) {
            self.socket = socket;

            self.myauth.connect(self.socket);

            if (!g.isFunction(self.__before)) {
                self.__before = function(psock, evt) {
                    return {
                        'data': psock,
                        'success': true
                    };
                };
            }

            /* variable injection via lambda function factory used in iteration / evil? */
            var factory = function(evt) {
                return function(sock) {

                    //console.log(socket.id);

                    var result = self.__before(sock, evt);
                    if (result.success) {

                        self.__execute[evt](result.data, socket);
                    }
                };
            };

            /* binding all methods */
            for (var evt in self.__execute) {
                socket.once(evt, factory(evt));
            }

            socket.on('disconnect', function(sock) {
                return function() {
                    self.myauth.disconnect(sock);
                };
            }(socket));

        });



        return true;
    };

    /**
     * @return instance of db model "name"
     */
    self.getModel = function(name, connection) {

        /**
         * get dbtype by name
         */

        dbtype = (typeof name.split('/')[0] !== 'undefined' && name.indexOf("/") !== -1) ? name.split('/')[0] : 'mysql';


        if (typeof connection === "undefined") {
            connection = self.db;
        }

        //console.log('../' + self.name + '/model/' + name, './model/' + dbtype);

        return new require('../' + self.name + '/model/' + name)(new require('./model/' + dbtype)(connection));
    };

    /**
     * init app
     */
    self.constructor = function(controller) {

        var init = [{
                'crowdguru': 'starting nd server'
            }];

        self.controller = controller;
        init.push({
            'initDB:': self.initDb(function(err, p_client) {

                init.push({
                    'initServer:': self.initServer()
                });

                init.push({
                    'initAuth:': self.initAuth(config, self.io)
                });

                init.push({
                    'bindMethods': self.bindMethods()
                });

                //console.log(init);
            })
        });
    };
    //self.constructor(); ONLY in child class
    return self;
};