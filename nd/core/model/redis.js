/**
 * nd server 2014 by alexander Kindziora
 * 
 */

module.exports = function(client) {
    var self = this; // public context
    self.db = client;

    self.errorHandler = function(data) {
        //console.log(data);
    };

    /**
     * update entry
     */
    self.update = function(entry, data, cb) {

    };

    /**
     * insert entry
     */
    self.insert = function(data, cb) {

    };

    /**
     * update or insert depends on id
     */
    self.upsert = function(data, cb, id) {
        id = (g.isset(id) ? id : 'id');
        if (g.isset(data[id])) {
            var q = {
                where: {}
            };

            q.where[id] = data[id];

            self.db.find(q).success(function(entry) {

                if (g.count(entry) > 0) {
                    self.update(entry, data, cb);
                } else {
                    self.insert(data, cb);
                }
            }).error(self.errorHandler);
        } else {
            self.insert(data, cb);
        }
    };

    /**
     * build sequelize model
     */
    self.constructor = function() {

        //console.log('model created ' + self.name);

        self.db.on("error", function(err) {
            //console.log("Error " + err);
        });


        return self;
    };

    return self;
};