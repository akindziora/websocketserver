/**
 * g server 2014 by alexander Kindziora
 * 
 */
module.exports = function(config, io) {
    var self = this;
    self.db = config.db;
    self.model = config.authmodel;
    self.session = {};
    
    //console.log(config, io);
    
    self.checkPermission = function(db) {

        return function(data, accept) {
            //console.log('checkPermission PERMISSION');

            // check if there's a cookie header
            if (data.headers.cookie) {
                // note that you will need to use the same key to grad the
                // session id, as you specified in the Express setup.

                if (data.headers.cookie.split('PHPSESSID=')[1]) {
                    self.SID = data.headers.cookie.split('PHPSESSID=')[1].split(';')[0];
                } else {
                    return accept('No sessionID transmitted.', false);
                }

                self.headers = data.headers;


                // //console.log(config.authmodel.db.find.toString());

                this.success = function(entry) {
                    if (entry) {
                        /* hack bis die serialize lösung "C" (class) und "o" (object) lesen kann */
                        self.entry = {
                            username: /\b((username\")(.*?)(";))\b/i.exec(entry)[3].split('"')[1],
                            id: /\b((sessionId\")(.*?)(";))\b/i.exec(entry)[3].split('"')[1],
                            guru_id: /\b((guruId\")(.*?)(";))\b/i.exec(entry)[3].split('"')[0].split(';i:')[1].split(';')[0]
                        };
 
                        if (!self.entry)
                            return accept('No valid session id found in database.', false);

                        self.session = self.entry;

                        if (!g.isset(self.session.username)) {
                            return accept('No valid session id found in database.', false);
                        }

                        // accept the incoming connection

                        return accept(null, true);
                    } else {
                        //console.log(data.headers);
                        return accept('No cookie transmitted.', false);
                    }

                };

                //mysql sql error or db error

                this.failed = function(e) {
                    return accept('error while fetching session against database', false);
                };

                db.find({
                    'where': {
                        'session_id': self.SID
                    }
                })
                        .success(this.success);

            } else {
                // if there isn't, turn down the connection with a message
                // and leave the function.
                return accept('No cookie transmitted.', false);
            }

        };

    };

    /**
     *
     */
    self.connect = function(socket) {

        //console.log('connect PERMISSION');
        if (g.isset(self.entry)) {
            if (typeof config.client[self.session.id] === 'undefined') {

                config.client[self.session.id] = {
                    'username': self.session.username,
                    'socket': {},
                    'session': self.SID,
                    'guruId': self.entry.guru_id
                };

                config.client[self.session.id].socket[socket.id] = self.headers;

            } else {
                config.client[self.session.id].socket[socket.id] = self.headers;
            }

            socket.set('phpsession', self.session.id, function() {
                //console.log('mitteilung an channel userliste', config.client);
            });

            io.sockets.in('userliste').emit('userliste', config.client);
            
            //console.log(io.sockets.clients(), config.client);
            
        }
    };

    /**
     * disconnect
     */
    self.disconnect = function(socket) {

        //console.log('DISCONNECT DELETE:' + socket.id);

        socket.get('phpsession', function(ull, sessionID) {
            
            //console.log(sessionID);
            
            config.client[sessionID].socket[socket.id] = null;

            delete config.client[sessionID].socket[socket.id];

            if (g.count(config.client[sessionID].socket) <= 0) {
                config.client[sessionID] = null;
                delete config.client[sessionID];
            }

            io.sockets.in('userliste').emit('userliste', config.client);
        });
 
        //config.socket.broadcast.emit('userlist', config.client);
    };

    /**
     * 
     */
    self.constructor = function() {

        //console.log('constructor PERMISSION');

        io.set('authorization', self.checkPermission(self.model.mydb));

        return self;
    };

    return self.constructor();
};