/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

global.wsclient = function(config) {
    var self = {};
    self.connect = function() {
        self.socket = io.connect('//' + window.location.hostname + '/', {
            'sync disconnect on unload': true,
            'port': 18111
        });

        self.socket.on('connecting', function() {
            console.log('connecting', arguments);
        });

        self.socket.on('disconnect', function() {
            console.log('disconnect', arguments);
        });

        self.socket.on('connect_failed', function() {
            console.log('connect_failed', arguments);
        });

        self.socket.on('error', function() {
            console.log('error', arguments);
        });

        self.socket.on('connect', function() {

            $(document).keyup(function(event) {
                self.socket.emit('ping', {'type': 'keyup', 'value': String.fromCharCode(event.keyCode)});
            });

        });


    };
    self.__callback = { /* 'testevent' : function (socket) {} */};
    /*
     * init sockserver
     */
    self.bindMethods = function() {

        for (var evt in self.__callback) {
            self.socket.on(evt, self.__callback[evt]);
        }

        return true;
    };
    self.connect();
    return self;
}();
