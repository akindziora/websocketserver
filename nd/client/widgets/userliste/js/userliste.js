/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var userliste = function() {
    
    var intfc = this;
    intfc.timer = {};
    /****ACTIONS*****************/
    this.actions = function(self) {
        self.namespace = 'user';
        self.__callback = {
            userliste: function(user) {
                var list = [], cnt = 0;
                var matchSite = function(url) {

                    var routes = {
                        'Jobliste': /\.de\/\?|app_dev.php\/\?/,
                        'Jobliste Backend': /\/jobbuilder\/jobmonitor\/?/,
                        'Bearbeitung': /\/jobbuilder\/jobmodule\/job\//,
                        'Briefing': /\/jobbuilder\/jobmodule\/show\//
                    }, site = 'andere';

                    $.each(routes, function(name, rex) {
                        var res = url.match(rex);
                        if (res) {
                            if (res.length > 0) {
                                var hasid = url.match(/\/(\d+)/);
                                site = name + ((hasid != null) ? " ID: " + hasid[1] : '');
                            }
                        }
                    });

                    return site;
                };

                var getURls = function(obj) {
                    var list = [], site;
                    $.each(obj, function(k, i) {
                        site = matchSite(i.referer);

                        intfc.timer[i.referer] = window.setTimeout(function() {
                            $('#userliste .label[data-url="' + i.referer + '"]').removeClass('label-success');
                        }, 1000);

                        list.push("<a href=\"#\" data-trigger=\"hover\" data-url=\"" + i.referer + "\" data-placement=\"right\" rel=\"popover\" data-content='" + i.referer + "'> <span class=\"label " + ((i.active) ? "label-success" : '') + "\">" + site + "</span></a> <a href=\"\" data-name='watch' data-id='" + k + "'></a>");
                    });
                    return list.join('<br/>');
                };

                var getBrowser = function(obj) {
                    var br = "";
                    $.each(obj, function(k, i) {
                        br = "<a href=\"#\" data-trigger=\"hover\" data-placement=\"left\" rel=\"popover\" data-content='" + i['user-agent'] + "'\" ><img style='height:16px' src='" + '//' + window.location.hostname + ":18111/widgets/userliste/img/browser.png' /></a>";
                    });
                    return br;
                };

                if ($('#userliste.in').get(0)) {
                    $.each(user, function(val, obj) {
                        cnt++;
                        var br = getBrowser(obj.socket),
                                site = getURls(obj.socket);

                        list.push("<tr><td><a href=\"#\" data-trigger=\"hover\" data-placement=\"right\" rel=\"popover\" data-content='Benutzer-ID: " + obj['guruId'] + "' ><strong>" + obj['username'] + "</strong></a></td><td>" + site + "</td><td><i class='fa fa-eye'></i> &nbsp;<i class='fa fa-envelope-o'></i></td><td>" + br + "</td></tr>");
                    });

                    $('[data-name="user"]').html(list.join(''));
                    $('[data-name="user"] a[rel="popover"]').popover();
                } else {
                    cnt = Object.keys(user).length;
                }

                $('[data-name="usercount"]').html(cnt);
            }
        };
        self.bindMethods();
        intfc.live = self;

        return {
            'user': self.__callback,
            'usercount': {
                'click': function() {
                    // intfc.live.socket.emit('join', 'userliste');
                    $('#userliste').modal('show'); 
                }
            },
            'close': {
                'click': function() {
                    // intfc.live.socket.emit('leave', 'userliste');
                }
            }
        };

    }(global.wsclient);
    /****MODEL*****************/
    this.model = {
        'field': {// here we declare model fields, with default values this is not strict default values are only used if we use directive: data-defaultvalues="client" on default we use server side default values because of the first page load
            'user': [
            ],
            'usercount': 0
        },
        'change': {},
        'changed': function() { //after model fields have changed

        }
    };

    /****VIEW*****************/
    this.view = {
        field: {
            usercount: function(value, $field) {
                return "<strong>" + value + "</strong>";
            }
        },
        views: {
            length: function(todos) {
                return todos.length;
            },
            userliste: function(user, $field) {
                var list = [];
                user.forEach(function(val, index) {
                    list.push("<tr><td><strong>" + val['username'] + "</strong></td><td><i>" + val['guruId'] + "</i></td><td>" + val['jobs'] + "</td></tr>")
                });
                return list.join('');
            }
        }
    };

    this._methods = {
        'init': function() {
            intfc.live.socket.emit('join', 'userliste');
            intfc.live.socket.emit('getClientList', {});
        }
    };
};
//var userclass = new userliste();

$('#nd-widget-gurusonline').klaster_(new userliste());


	