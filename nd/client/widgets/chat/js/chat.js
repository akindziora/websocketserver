/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var userliste = function() {
    var intfc = this;

    /****ACTIONS*****************/
    this.actions = function(self) {
        self.namespace = 'user';
        self.__callback = {
            clientlist: function(user) {
                var list = [], cnt = 0;
                
                $.each(user, function(val, obj) {
                    cnt++;
                    list.push("<tr><td><strong>" + obj['username'] + "</strong></td><td><i>" + obj['guruId'] + "</i></td><td>" + obj['jobs'] + "</td></tr>");
                });

                $('[data-name="user"]').html(list.join(''));
                $('[data-name="usercount"]').html(cnt);
                 
            }
        };
        self.bindMethods();
        intfc.live = self;

        return {
            'user': self.__callback,
            'usercount': {
                'click': function() {
                    $('#userliste').modal('show');
                }
            }
        };

    }(new client());


    /****MODEL*****************/
    this.model = {
        'field': {// here we declare model fields, with default values this is not strict default values are only used if we use directive: data-defaultvalues="client" on default we use server side default values because of the first page load
            'user': [
            ],
            'usercount': 0
        },
        'change': {},
        'changed': function() { //after model fields have changed

        }
    };

    /****VIEW*****************/
    this.view = {
        field: {
            usercount: function(value, $field) {
                return "<strong>" + value + "</strong>";
            }
        },
        views: {
            length: function(todos) {
                return todos.length;
            },
            userliste: function(user, $field) {
                var list = [];
                user.forEach(function(val, index) {
                    list.push("<tr><td><strong>" + val['username'] + "</strong></td><td><i>" + val['guruId'] + "</i></td><td>" + val['jobs'] + "</td></tr>")
                });
                return list.join('');
            }
        }
    };

    this._methods = {
        'init': function() {
            intfc.live.socket.emit('getClientList', {});
        }
    };

};

//var userclass = new userliste();

$('#nd-widget-gurusonline').klaster_(new userliste());